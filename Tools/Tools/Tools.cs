﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
    public static class Tools
    {
        //Async function in sync context can deadlock on .Result call.
        //This function call the async function 'func' with arg 'arg' in task run to not deadlock.
        //We get a clean synchrony return on return function type.

        #region SafeResult
        public static TRet SafeResult<TRet, T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6, Task<TRet>> func, T1 arg, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            return Task.Run(() => func(arg, arg2, arg3, arg4, arg5, arg6)).Result;
        }

        public static TRet SafeResult<TRet, T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5, Task<TRet>> func, T1 arg, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            return Task.Run(() => func(arg, arg2, arg3, arg4, arg5)).Result;
        }

        public static TRet SafeResult<TRet, T1, T2, T3, T4>(Func<T1, T2, T3, T4, Task<TRet>> func, T1 arg, T2 arg2, T3 arg3, T4 arg4)
        {
            return Task.Run(() => func(arg, arg2, arg3, arg4)).Result;
        }


        public static TRet SafeResult<TRet, T1, T2, T3>(Func<T1, T2, T3, Task<TRet>> func, T1 arg, T2 arg2, T3 arg3)
        {
            return Task.Run(() => func(arg, arg2, arg3)).Result;
        }

        public static TRet SafeResult<TRet, T1, T2>(Func<T1, T2, Task<TRet>> func, T1 arg, T2 arg2)
        {
            return Task.Run(() => func(arg, arg2)).Result;
        }

        public static TRet SafeResult<TRet, Targ>(Func<Targ, Task<TRet>> func, Targ arg)
        {
            return Task.Run(() => func(arg)).Result;
        }

        public static TRet SafeResult<TRet>(Func<Task<TRet>> func)
        {
            return Task.Run(func).Result;
        }
        #endregion

        #region PrettyApiCall
        public class PrettyApiCall
        {
            private static HttpClient _client;

            public PrettyApiCall(HttpClient c)
            {
                _client = c;
            }

            public async Task<TRes> Read<TRes>(HttpResponseMessage res)
            {
                if (res.IsSuccessStatusCode)
                {
                    var result = await res.Content.ReadAsAsync<TRes>();
                    return result;
                }
                return default;
            }

            public async Task<TRes> GetAndRead<TRes>(string endpoint)
            {
                var response = await _client.GetAsync(endpoint);
                return await Read<TRes>(response);
            }

            public async Task<TRes> PostAndRead<TPost, TRes>(TPost element, string endpoint)
            {
                var response = await _client.PostAsJsonAsync(endpoint, element);
                return await Read<TRes>(response);
            }

            public async Task<TRes> DeleteAndRead<TRes>(string endpoint)
            {
                var response = await _client.DeleteAsync(endpoint);
                return await Read<TRes>(response);
            }
        }
        #endregion

        #region currying

        public static Func<T1, Func<T2, Func<T3, Func<T4, TRes>>>> curry<T1, T2, T3, T4, TRes>(Func<T1, T2, T3, T4, TRes> function)
        {
            return t1 => t2 => t3 => t4 => function(t1, t2, t3, t4);
        }

        public static Func<T1, Func<T2, Func<T3, TRes>>> curry<T1, T2, T3, TRes>(Func<T1, T2, T3, TRes> function)
        {
            return t1 => t2 => t3 => function(t1, t2, t3);
        }

        public static Func<T1, Func<T2, TRes>> curry<T1, T2, TRes>(Func<T1, T2, TRes> function)
        {
            return t1 => t2 => function(t1, t2);
        }


        #endregion

        #region ApplyTuple

        public static TRes ApplyTuple<T1, T2, T3, TRes>(Tuple<T1, T2, T3> t, Func<T1, T2, T3, TRes> function)
        {
            dynamic tmpFunc = curry<T1, T2, T3, TRes>(function);
            var values = t.GetType().GetProperties().Select(property => property.GetValue(t));
            foreach (dynamic value in values)
            {
                tmpFunc = tmpFunc(value);
            }

            return tmpFunc;
        }

        public static TRes ApplyTuple<T1, T2, TRes>(Tuple<T1, T2> t, Func<T1, T2, TRes> function)
        {
            dynamic tmpFunc = curry<T1, T2, TRes>(function); // currying to make the function able to be call on "one by one" parameter mode

            var values = t.GetType().GetProperties().Select(property => property.GetValue(t));
            foreach (dynamic value in values) // iterate on tuple values
            {
                tmpFunc = tmpFunc(value); // call function with current param and stock the partial application as himself to be repeated
            }

            return tmpFunc; //the last call product the value and not a function so we can return it
        }

        #endregion
    }
}
